import requests
from bs4 import BeautifulSoup
import csv
import time
from requests.exceptions import RequestException

# Function to fetch a page with error handling
def fetch_page(url):
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36'
    }
    
    retries = 5
    for i in range(retries):
        try:
            response = requests.get(url, headers=headers)
            response.raise_for_status()  # Raise an error for bad responses
            return response.text
        except RequestException as e:
            print(f"Error fetching {url}: {e}")
            time.sleep(2 ** i)  # Exponential backoff
    return None

# Main scraping function
def scrape_egydir():
    base_url = "https://www.egydir.com/ar/listings/factory?city=212&page="
    data = []
    
    for page in range(1, 35):  # Iterate from page 1 to 34
        print(f"Scraping page {page}...")
        url = f"{base_url}{page}"
        html = fetch_page(url)
        
        if html is None:
            continue  # Skip to the next page if there was an error
        
        soup = BeautifulSoup(html, 'html.parser')
        
        # Find all listings
        listings = soup.find_all('div', class_='list-title')  # Adjust selector as needed
        
        for listing in listings:
            try:
                name = listing.text.strip()
                phone = listing.find_next('div', class_='col-lg-4 col-md-4 col-sm-6 list-icon').text.strip()
                website = listing.find_next('div', class_='col-lg-4 col-md-4 col-sm-6 list-icon').find_next('a')['href']
                
                data.append({
                    'Company Name': name,
                    'Phone Number': phone,
                    'Website': website
                })
            except Exception as e:
                print(f"Error parsing listing: {e}")

        time.sleep(2)  # Rate limiting: wait for 2 seconds between requests

    # Save data to CSV
    keys = data[0].keys() if data else []
    with open('egydir_data.csv', 'w', newline='', encoding='utf-8') as output_file:
        dict_writer = csv.DictWriter(output_file, fieldnames=keys)
        dict_writer.writeheader()
        dict_writer.writerows(data)

    print(f"Scraping complete! {len(data)} records saved to egydir_data.csv.")

# Run the scraper
if __name__ == "__main__":
    scrape_egydir()
